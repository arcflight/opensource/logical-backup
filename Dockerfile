FROM registry.opensource.zalan.do/library/ubuntu-22.04:latest

ENV PG_DIR=/usr/lib/postgresql

SHELL ["/bin/bash", "-o", "pipefail", "-c"]

RUN mkdir -p /var/cache/apt/archives/partial && \
    apt-get update && \
    apt-get install --no-install-recommends -y \
        apt-utils \
        ca-certificates \
        lsb-release \
        pigz \
        python3-pip \
        python3-setuptools \
        curl \
        jq \
        gnupg \
        gcc \
        libffi-dev && \
    curl -sL https://aka.ms/InstallAzureCLIDeb | bash && \
    pip3 install --upgrade pip && \
    pip3 install --no-cache-dir awscli --upgrade && \
    pip3 install --no-cache-dir gsutil --upgrade && \
    sh -c 'echo "deb http://apt.postgresql.org/pub/repos/apt $(lsb_release -cs)-pgdg main" > /etc/apt/sources.list.d/pgdg.list' && \
    curl -fsSL https://www.postgresql.org/media/keys/ACCC4CF8.asc | gpg --dearmor -o /etc/apt/trusted.gpg.d/postgresql.gpg && \
    mkdir -p /var/lib/apt/lists/partial && \
    apt-get update && \
    apt-get install --no-install-recommends -y \
        postgresql-client-16 \
        postgresql-client-15 \
        postgresql-client-14 \
        postgresql-client-13 \
        postgresql-client-12 && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/* && \
    rm -rf /var/cache/apt/archives/*

COPY boto.cfg /etc/boto.cfg
COPY dump.sh ./dump.sh
RUN chmod +x dump.sh

ENTRYPOINT ["/dump.sh"]